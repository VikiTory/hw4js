/*---Теоретичні питання---
1) Описати своїми словами навіщо потрібні функції у програмуванні.
Функції дозволяють робити однакові дії багато разів без повторення коду.
2) Описати своїми словами, навіщо у функцію передавати аргумент.
За допомогою arguments можна викликати функцію, передаючи в неї більше аргументів,
ніж формально оголосили прийняти (це корисно, коли не відомо скільки аргументів має прийняти
функція),а також отримати доступ до кожного аргументу.
3) Що таке оператор return та як він працює всередині функції?
Оператор return завершує виконання поточної функції та повертає її значення.


/*---Практичне завдання---*/
    let num1;
    let num2;
    let result;
    
    do {
        num1 = prompt("Enter first number:");
    } while (num1 === "" || isNaN(num1) || num1 === null);

    do {
        num2 = prompt("Enter second number:");
    } while (num2 === "" || isNaN(num2) || num2 === null);

    let mathOperation = prompt("mathOperation +,-,*,%");

function numberOperation(num1, num2, operator) {
    console.log(operator);

    switch (operator) {
        case '+':
            console.log(num1 + num2);
            break;
        case '-':
            console.log(num1 - num2);
            break;
        case '*':
            console.log(num1 * num2);
            break;
        case '/':
            console.log(num1 / num2);
            break;
        default:
            return 'Invalid operation';
            break;
    }
}
numberOperation(num1, num2,mathOperation);
    
    